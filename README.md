# uDash

uDash (micro-dash) - Show live-dashboards in your terminal, for quick-overview of all the important information to you and easy troubleshooting when problem arises

Basically its a single-file program, that will show a dashboard in the terminal, and that dashboard is composed of "rectangles" where any-bash-command is continuously executed and shown (similar to the "watch" command)

Every 1sec, the commands of each brick will be executed in background threads (more precisely, go-routines wich are much more resource-efficient than linux kernel-threads), and once the commands terminate their stdout/stderr is then shown (updated) in the terminal-dashboard. 

A few shortcut-keys keys allow to go-next/prev-page and quit.  
However, its not designed to interact with the bricks/bash-commands while the dashboard is running - think of the dashboard as a "pane of glass" that shows the stdout/stderr of commands. If you need to interact with the commands while they are executing, then have a look at [tmux](https://github.com/tmux/tmux/wiki), it will be a better fit for that use-case.


* A dashboard can contain multiple "pages", each page will show multiple (lines/columns) bricks, each brick is the stdout/stderr of its bash-commands (whetever you want)
* Supports regexp highlighting (ex: "error" painted in red, "[789].%" painted in yellow, you defined the regexp and background/foregorund color you want)
* Made in go, single-file executable: no dependecies, no install, just copy and run in any other linux distro. So easy to use in restricted/production servers when problem arise, to enable a complete vision of multiple parameters during a complex troubleshooting situation.



## Tell me more about those commands

Commands that execute faster than 1 sec, will be executed and then have to wait until the end-of-the-second, when its stdout/stderr will be updated and shown in the terminal-dashboard, and the command re-executed again at the start-of-next-second. 

Commands that execute longer that 1 sec (ex: "sleep 5; date"), will be executed and while executing its stdout/stderr is not updated in the terminal (but a time-delay-counter is shown), but once the execution finishes in the next end-of-second its stdout/stderr will be updated and shown in the terminal-dashboard, and the command re-executed again at the start-of-the-next-second.

Also, in each brick there is an activity-indicator (or time-delay-counter) to make visible when a brick/command has been updated or is stalled taking a long time




## Usage:

```bash
./uDash ./mydash_networkingStatus.yml
./uDash /full/path/mydash_networkingStatus.yml
./uDash                                         # same as ./uDash ~/.config/uDash/uDash.template.yml
./uDash otherDash.yml                           # same as ./uDash ~/.config/uDash/otherDash.yml
```
It can be used to check kernel/net/disk/resources-stats, application-stats, logfile-tracking - to show whatever comes from stdout/stderr of bash commands


### Dashboard keys

```
  <Left>/<Right>         : prev/next page
  <Escape>, q, Ctrl-C    : quit
```

All other keys are ignored   


## How to create your dashboards

The **~/.config/uDash/uDash.template.yml** file is an example with an extensive description of how the terminal is split into lines/cols/bricks, and for each brick what are the  bash-commands that should be executed and shown in each brick

### For your first dashboard:

+ First run once ./uDash, so it creates the default TEMPLATE_FILE ([1])
+ Copy the TEMPLATE_FILE file ([1]) to your own ./myDash.yml, like   
  `$ cp -v ~/.config/uDash/uDash.template.yml ./myDash.yml`
+ Edit ./myDash.yml and add/substitute the *UserCommand* by your own commands
+ Run it with   
  `./uDash ./myDash.yml`  
  Every 1sec, the commands will be executed in background, and once they terminate their stdout/stderr gets shown (updated) in the dashboard


### For your second dashboard, an improved one, step3 becomes more refined:

+ First think about the *design* you want for the dashboard: 
  + Decide what *UserCommand* you want to show, and have a rough idea of how much space they will need
  + Decide how many *pages* you want
  + Decide how many *bricks* you want in each *page*, to be able to show those UserCommand's
  + For each *page*, decide *where* you want to place each *bricket*
  + For each *page*, decide the *size* (*Height_percentOfTerminal* and *Width_span1to12*) of each *bricket*
  + For each *page*, calculate the *lines* and *cols* needed to place those *brickets*
  Its usually easier to think this with a pencil-and-paper
+ And finally, edit the .yml file and put the resulting values there
  Iterate a couple of times, to adjust the sizes, commands, and ordering
+ (Optional) Add *Highlights* to the bricks (regexp,fg-color,bg-color), like this for example:
  + red   for "error or failed"
  + green for <= "10%"
  + blue  for "intERESTing (ignore case)"


## Known limitations
+ TABs "\t" are not displayed correctly (are deleted)  
  Possible hacky workaround: " yourCommandWithTabsInStdout | tr '\t' ' '"

## Related files
[1] - ~/.config/uDash/uDash.template.yml





