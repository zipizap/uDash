/*
  See notes in freeplane:/%20/C:/Users/ecampau/Dropbox/projs/vps5/vps5.mm#ID_1027314975
*/

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/exec"
	"os/user"
	"regexp"
	"strconv"
	"strings"
	"time"

	// go get -u github.com/gizak/termui

	ui "github.com/gizak/termui"
	"github.com/olebedev/config"
)

/*
	// create once, its a singleton
	_ := Init_BetterYamlConfig_Singleton("./myConfig.yml")
	// once created, never again call Init_BetterYamlConfig_Singleton()

	// the config is stored in a global var BetterYamlConfig_Singleton, so it can be used from anywhere in the code
	BetterYamlConfig_Singleton
	BetterYamlConfig_Singleton.cfg

*/
type BetterYamlConfig struct {
	cfg *config.Config
}

var BetterYamlConfig_Singleton BetterYamlConfig

func Init_BetterYamlConfig_Singleton(yaml_filename string) (*BetterYamlConfig, error) {
	if BetterYamlConfig_Singleton.cfg != nil {
		// BetterYamlConfig_Singleton is already initialized
		return &BetterYamlConfig_Singleton, nil
	}
	BetterYamlConfig_Singleton = BetterYamlConfig{}
	var err error
	BetterYamlConfig_Singleton.cfg, err = config.ParseYamlFile(yaml_filename)
	if err != nil {
		return nil, err
	}
	return &BetterYamlConfig_Singleton, nil
}

/*
  // create
	_ = Init_Pages_Singleton()
	// global var Pages_Singleton is now defined
	Pages_Singleton.AddPage( &new_page )
	Pages_Singleton.AddPage( &new_page )
	...

	// get active page
	active_page := Pages_Singleton.Get_ActivePage()

	// navigate
	next_page := Pages_Singleton.CycleNextPageBecomesActive()
	prev_page := Pages_Singleton.CyclePrevPageBecomesActive()

*/
type Pages struct {
	list             []*Page
	activePage_index int
}

var Pages_Singleton Pages

func Init_Pages_Singleton() (*Pages, error) {
	Pages_Singleton = Pages{}
	Pages_Singleton.activePage_index = 0

	byc := &BetterYamlConfig_Singleton
	cfg_pages, err := byc.cfg.Get("")
	if err != nil {
		return nil, err
	}

	list_of_pages, err := cfg_pages.List("")
	if err != nil {
		return nil, err
	}

	for h, _ := range list_of_pages {
		// Page
		cfg_page, err := cfg_pages.Get(strconv.Itoa(h) + ".page")
		if err != nil {
			return nil, err
		}
		log.Print(err)

		page_g := [][][]*BeaconCmdPar{}
		list_of_lines, err := cfg_page.List("")
		if err != nil {
			return nil, err
		}

		for i, _ := range list_of_lines {
			// Line
			cfg_line, err := cfg_page.Get(strconv.Itoa(i) + ".line")
			if err != nil {
				return nil, err
			}

			g_line := [][]*BeaconCmdPar{}
			list_of_cols, err := cfg_line.List("")
			if err != nil {
				return nil, err
			}

			for j, _ := range list_of_cols {
				// Col
				cfg_col, err := cfg_line.Get(strconv.Itoa(j) + ".col")
				if err != nil {
					return nil, err
				}

				g_col := []*BeaconCmdPar{}
				list_of_bricks, err := cfg_col.List("")
				if err != nil {
					return nil, err
				}

				for s, _ := range list_of_bricks {
					// We have:
					//		y_brick						map[string]interface{} from yaml/config
					//		g_bcmdpar_ptr			*BeaconCmdPar
					// and we will fill in g_bcmdpar_ptr with the values from y_brick

					y_brick, err := cfg_col.Map(strconv.Itoa(s) + ".brick")
					if err != nil {
						return nil, err
					}

					// - we can now read y_brick.BorderLabel etc

					g_bcmdpar_ptr := &BeaconCmdPar{}
					// - can can now write g_bcmdpar_ptr.P.xxx etc

					g_bcmdpar_ptr.P = ui.NewPar("...executing command for first time...")
					g_bcmdpar_ptr.ud.BorderLabel = y_brick["BorderLabel"].(string)
					g_bcmdpar_ptr.ud.UserCommand = y_brick["UserCommand"].(string)
					g_bcmdpar_ptr.ud.Span = y_brick["Width_span1to12"].(int)
					g_bcmdpar_ptr.ud.Height_percent = y_brick["Height_percentOfTerminal"].(int)
					// Highlights if they exist
					if val, ok := y_brick["Highlights"].([]interface{}); ok {
						g_bcmdpar_ptr.ud.Highlights = val
					}
					g_bcmdpar_ptr.UiUpdate()

					g_brick := g_bcmdpar_ptr
					g_col = append(g_col, g_brick)
				}
				g_line = append(g_line, g_col)
			}
			page_g = append(page_g, g_line)
		}
		// At this point, page_g [][][]*BeaconCmdPar is filled
		a_page := NewPage(&page_g)
		Pages_Singleton.AddPage(a_page)
	}
	// At this point,each page was added, one by one

	return &Pages_Singleton, nil
}

func (self *Pages) AddPage(new_page *Page) *Pages {
	if len(self.list) == 0 {
		// Page being added for first time (first page)
		// init self.activePage_index
		self.activePage_index = 0
	}
	self.list = append(self.list, new_page)
	return self
}
func (self *Pages) Get_ActivePage() *Page {
	return self.list[self.activePage_index]
}
func (self *Pages) CycleNextPageBecomesActive() (nextPage *Page) {
	self.activePage_index = int(math.Mod(float64(self.activePage_index+1), float64(len(self.list))))
	nextPage = self.Get_ActivePage()
	return nextPage
}
func (self *Pages) CyclePrevPageBecomesActive() (prevPage *Page) {
	self.activePage_index = int(math.Mod(float64(self.activePage_index+len(self.list)-1), float64(len(self.list))))
	prevPage = self.Get_ActivePage()
	return prevPage
}

func (self *Pages) UiShow_ActivePage() {
	// UiUpdate ActivePage
	current_page := Pages_Singleton.Get_ActivePage()
	current_page.UiUpdate()
}

/*
	// create
	a_pg := NewPage(g_ptr)


	// helper func
	a_pg.Each_LineColBrick(fuu)

*/
type Page struct {
	g        [][][]*BeaconCmdPar
	BodyGrid *ui.Grid
}

func NewPage(g_ptr *[][][]*BeaconCmdPar) *Page {
	// helper func

	new_page := &Page{}
	new_page.g = *g_ptr
	_ = new_page.Recalculate_BodyGrid_from_g()
	return new_page
}

func (self *Page) Recalculate_BodyGrid_from_g() *Page {
	//NOTE: maybe this func can be embedded inside NewPage, if its not used elsewhere...
	// from self.g recalculate self.BodyGrid

	// uses gpage [][][]*BeaconCmdPar
	gpage := self.g
	the_rows := []*ui.Row{}
	{
		for i, _ := range gpage {
			the_cols := []*ui.Row{}
			for j, _ := range gpage[i] {
				//the_bricks := []*BeaconCmdPar{}
				the_bricks_spans := []int{}
				the_bricks_widgets := []ui.GridBufferer{}
				for s, _ := range gpage[i][j] {
					the_brick := gpage[i][j][s]
					//the_bricks = append(the_bricks, the_brick)

					the_bricks_spans = append(the_bricks_spans, the_brick.ud.Span)
					the_bricks_widgets = append(the_bricks_widgets, the_brick.P)
				}

				the_stack_maxSpan := maxIntOf(the_bricks_spans)
				the_col := ui.NewCol(the_stack_maxSpan, 0, the_bricks_widgets...)
				the_cols = append(the_cols, the_col)
			}

			the_row := ui.NewRow(the_cols...)
			the_rows = append(the_rows, the_row)
		}
	}
	// at this point, the_rows is defined

	// setup self.BodyGrid
	self.BodyGrid = ui.NewGrid(the_rows...)
	self.UiUpdate() //implicitly makes some needed settings on self.BodyGrid
	// at this pint, self.BodyGrid is defined

	return self
}

func (self *Page) UiUpdate() *Page {
	// update widgets
	self.Each_LineColBrick(func(bcmdp *BeaconCmdPar) {
		bcmdp.UiUpdate()
	})

	return self
}

func (self *Page) Each_LineColBrick(fuu func(bcmdpar_ptr *BeaconCmdPar)) {
	page := self.g
	for i, _ := range page {
		for j, _ := range page[i] {
			for s, _ := range page[i][j] {
				fuu(page[i][j][s])
			}
		}
	}
}

/*
  // create
  ud := Userdata{}
  ud.BorderLabel     = ...
  ud.UserCommand     = ...
  ud.Span            = ...
  ud.Height_percent  = ...
	ud.Highlights      = ...
  ud.other           = ...

  // ud.Height() exists instead of ud.Height
  height := ud.Height()
*/
type Userdata struct {
	BorderLabel    string
	UserCommand    string
	Span           int                    // 1-12
	Height_percent int                    // 1-100
	Highlights     []interface{}          // [] empty or not (see len(s) == 0)
	other          map[string]interface{} // for whatever needed
}

func (self *Userdata) Height() int {
	height := int(math.Ceil(float64(self.Height_percent)/100*float64(ui.TermHeight()))) - 1
	return height
}

/*
  // Border Tip indicating when command was last updated

  // create
	bTip := NewBorderTipLastUpdated("original border text here")

	// refresh at 1s-handler
	bTip.refresh(bool_command_was_just_updated_and_timer_should_be_reset_now)

*/
type BorderTipLastUpdated struct {
	timeLastUpdate        time.Time
	prefix                string
	BorderLabelOriginal   string
	BorderLabelWithPrefix string
}

func NewBorderTipLastUpdated(the_borderLabelOriginal string) *BorderTipLastUpdated {
	btip := BorderTipLastUpdated{}
	btip.BorderLabelOriginal = the_borderLabelOriginal
	btip.Refresh(true)
	return &btip
}
func (self *BorderTipLastUpdated) Refresh(bool_command_was_just_updated_and_timer_should_be_reset_now bool) {
	// helper func to calculate BorderLabelWithPrefix
	calculateAndSet_BorderLabelWithPrefix := func() {
		self.BorderLabelWithPrefix = self.prefix + " " + self.BorderLabelOriginal
	}

	// Initialization of self
	{
		if self.timeLastUpdate.IsZero() {
			self.timeLastUpdate = time.Now()
			self.prefix = "+"
			calculateAndSet_BorderLabelWithPrefix()
			return
		}
	}

	// Refresh: update self.prefix and self.BorderLabelWithPrefix
	if bool_command_was_just_updated_and_timer_should_be_reset_now {
		self.timeLastUpdate = time.Now()

		//  self.prefix =
		// 		+    >>   -
		// 		-    >>   +
		// [XXy]   >>   +
		if self.prefix == "+" {
			self.prefix = "-"
		} else if self.prefix == "-" {
			self.prefix = "+"
		} else {
			self.prefix = "+"
		}

		calculateAndSet_BorderLabelWithPrefix()

	} else {
		// false: bool_command_was_just_updated_and_timer_should_be_reset_now
		// Ie, when we are still waiting, and should update the tip with indication of lapsedTime
		//
		// self.timeLastUpdate unchanged
		//
		// prefix changes depending on lapsedTime:
		//				lapsedTime					old-prefix				new-prefix
		//						<10s						(whatever)				(no-change)
		//				    >10s						(whatever)				[XXhXXmXXs]
		lapsedTime := time.Since(self.timeLastUpdate).Round(time.Second)
		if lapsedTime >= (time.Second * 10) {
			self.prefix = fmt.Sprintf("[%v]", lapsedTime)
			calculateAndSet_BorderLabelWithPrefix()
		}
	}

}

/*
   // create
   bcmdpar           := BeaconCmdPar{}
   bcmdpar.P          = my_uiPar
   bcmdpar.ud         = my_ud
   bcmdpar.UiUpdate()

	 // in eventhandler of /sys/wnd/resize, add a
	 bcmdpar.UiUpdate()

   // in eventhandler of time, add a
	 bcmdpar.UiUpdate()
*/
type BeaconCmdPar struct {
	P           *ui.Par
	channel     chan string
	bTipLastUpd *BorderTipLastUpdated
	ud          Userdata
}

func (self *BeaconCmdPar) UiUpdate() (widget_was_updated bool) {
	// widget_was_updated signals if the widget was modified
	widget_was_updated = false

	// The first time this function is called, it should make initializations and apply defaults
	// The second, third, ... times this function is called, it should update only
	// the essential needed and very fast, to dont build delay/flicker in the ui

	if self.channel == nil {
		// First time this function is called: self.channel is nil

		// Update widget (Par) with:
		//   a) userdata
		{
			if self.P.BorderLabel != self.ud.BorderLabel {
				self.P.BorderLabel = self.ud.BorderLabel
				widget_was_updated = true
			}
			self_id_Height := self.ud.Height()
			if self.P.Height != self_id_Height {
				self.P.Height = self_id_Height
				widget_was_updated = true
			}
			//self.P.Width			 // this value is set automatically by the layout
		}

		//   b) defaults
		{
			default_self_P_TextFgColor := ui.ColorWhite
			default_self_P_BorderFg := ui.ColorCyan
			if self.P.TextFgColor != default_self_P_TextFgColor {
				self.P.TextFgColor = default_self_P_TextFgColor
				widget_was_updated = true
			}
			if self.P.BorderFg != default_self_P_BorderFg {
				self.P.BorderFg = default_self_P_BorderFg
				widget_was_updated = true
			}
		}

		// Init channel and go-routine
		self.channel = make(chan string, 0)
		go func(bcmdp *BeaconCmdPar) {
			channel := bcmdp.channel
			// infinite loop
			for {
				// exec user_command
				cmd := exec.Command("/bin/bash", "-c", bcmdp.ud.UserCommand)
				stdoutStderr_bytes, err := cmd.CombinedOutput()
				var stdoutStderr string
				if err != nil {
					stdoutStderr = "Error: " + err.Error() + "\n"
				} else {
					stdoutStderr = string(stdoutStderr_bytes)

					// Highlights (if they exist)
					for i := range bcmdp.ud.Highlights {
						hi_map := bcmdp.ud.Highlights[i].(map[string]interface{})
						// hi_map :=> map[string]interface{}
						re := regexp.MustCompile(hi_map["MatchRegexp"].(string))
						stdoutStderr = re.ReplaceAllStringFunc(stdoutStderr, func(a_match string) string {
							// [text highlighted](fg-red,fg-bold,bg-blue)
							a_match_colored_with_markdown := "[" + a_match + "]" + "(" + hi_map["FgColor"].(string) + "," + hi_map["BgColor"].(string) + ")"
							//lllog(a_match_colored)
							return a_match_colored_with_markdown
						})
					}
				}

				//lllog(stdoutStderr)
				channel <- stdoutStderr
				//time.Sleep(time.Duration(bcmdp.Refresh_ms) * time.Millisecond)
			}
		}(self)

		// Init bTipLastUpd
		self.bTipLastUpd = NewBorderTipLastUpdated(self.ud.BorderLabel)
	}

	// Second, third, ... every-other time (not-first) this func is called

	// Update widget (Par) with only the essentials and very fast
	// a) Text with UserCommand
	ui_update_text_from_channel := func(bcmdp *BeaconCmdPar) bool {
		getValueOrSkip := func(c chan string) (string, bool) {
			var value string
			var hasValue bool
			select {
			case value = <-c:
				hasValue = true
			default:
				hasValue = false
			}
			return value, hasValue
		}

		cmdStdoutStderr, hasValue := getValueOrSkip(bcmdp.channel)
		//lllog(">> " + cmdStdoutStderr + ": " + strconv.FormatBool(hasValue))

		// Update bcmdp.ud.BorderLabel
		bcmdp.bTipLastUpd.Refresh(hasValue)
		bcmdp.ud.BorderLabel = bcmdp.bTipLastUpd.BorderLabelWithPrefix
		bcmdp.P.BorderLabel = bcmdp.ud.BorderLabel

		// Update Text
		if hasValue {
			bcmdp.P.Text = cmdStdoutStderr
		}
		return hasValue
	}

	widget_was_updated = ui_update_text_from_channel(self)

	return widget_was_updated
}

var UI_IS_CLOSED bool = false

func ui_StopClose() {
	if UI_IS_CLOSED == true {
		return
	}
	ui.StopLoop()
	ui.Close()
	UI_IS_CLOSED = true
	// now stdout and stderr are back to "normal"
	return
}
func ui_StopClose_logPrint(v interface{}) {
	ui_StopClose()
	log.Print(v)
}
func ui_StopClose_logPanic(v interface{}) {
	ui_StopClose()
	log.Panic(v)
}

func main() { os.Exit(mainReturnWithCode()) }
func mainReturnWithCode() int {
	// defer to convert panics into (non-zero) exit-code
	// NOTE: this must be the *first* "defer" of mainReturnWithCode, to be at the "bottom" of the defer-stack (last executed, FILO)
	defer func() {
		if r := recover(); r != nil {
			ui_StopClose_logPanic(r)
			os.Exit(100)
		}
	}()

	// Init ui
	{
		err := ui.Init()
		if err != nil {
			ui_StopClose_logPrint(err)
			return 98
		}
		UI_IS_CLOSED = false
		defer ui_StopClose()
	}

	// ========================
	// Init widgets and grid layout
	{
		yaml_filename, err := getYamlFilenameFromArgs()
		if err != nil {
			ui_StopClose_logPrint(err)
			return 95
		}

		// Init BetterYamlConfig_Singleton
		_, err = Init_BetterYamlConfig_Singleton(yaml_filename)
		if err != nil {
			ui_StopClose_logPrint(err)
			return 99
		}

		// Init Pages_Singleton (uses BetterYamlConfig_Singleton)
		_, err = Init_Pages_Singleton()
		if err != nil {
			ui_StopClose_logPrint(err)
			return 96
		}

		// At this point we have defined and ready:
		//	Pages_Singleton, with all its underlaying [] Page.BodyGrid

		// clear and recreate ui
		Pages_Singleton.UiShow_ActivePage()
		// update BodyGrid (adjust width to look well on resizes)
		Pages_Singleton.Get_ActivePage().BodyGrid.Width = ui.TermWidth()
		Pages_Singleton.Get_ActivePage().BodyGrid.Align()
		// show in ui
		ui.Render(Pages_Singleton.Get_ActivePage().BodyGrid)
	}

	// ========================
	// Define event handlers, including timer to call updates
	// - handle key to quit
	{
		func_stop := func(ui.Event) {
			ui_StopClose()
		}
		ui.Handle("/sys/kbd/q", func_stop)
		ui.Handle("/sys/kbd/C-c", func_stop)
		ui.Handle("/sys/kbd/<escape>", func_stop)
	}
	// - handle next/prev page - TAB/C-tab, right/left
	{
		func_next_page := func(ui.Event) {
			Pages_Singleton.CycleNextPageBecomesActive()
			// Update and show active page
			Pages_Singleton.UiShow_ActivePage()
			// update BodyGrid (adjust width to look well on resizes)
			Pages_Singleton.Get_ActivePage().BodyGrid.Width = ui.TermWidth()
			Pages_Singleton.Get_ActivePage().BodyGrid.Align()
			ui.Clear()
			ui.Render(Pages_Singleton.Get_ActivePage().BodyGrid)
		}
		func_prev_page := func(ui.Event) {
			Pages_Singleton.CyclePrevPageBecomesActive()
			// Update and show active page
			Pages_Singleton.UiShow_ActivePage()
			// update BodyGrid (adjust width to look well on resizes)
			Pages_Singleton.Get_ActivePage().BodyGrid.Width = ui.TermWidth()
			Pages_Singleton.Get_ActivePage().BodyGrid.Align()
			ui.Clear()
			ui.Render(Pages_Singleton.Get_ActivePage().BodyGrid)
		}
		ui.Handle("/sys/kbd/<right>", func_next_page)
		ui.Handle("/sys/kbd/<space>", func_next_page)
		ui.Handle("/sys/kbd/<left>", func_prev_page)
		ui.Handle("/sys/kbd/<backspace>", func_prev_page)
	}

	// - handle resize
	ui.Handle("/sys/wnd/resize", func(ui.Event) {
		// Update and show active page
		Pages_Singleton.UiShow_ActivePage()

		// Update all widgets Height
		{
			// This is an expensive op, so its not done on typical UiUpdate()'s
			// (where it would produce flicker) but only here, when needed
			for _, pg := range Pages_Singleton.list { // pg *Page
				pg.Each_LineColBrick(func(bcmdp *BeaconCmdPar) {
					bcmdp.P.Height = bcmdp.ud.Height()
				})
			}
		}

		// update BodyGrid (adjust width to look well on resizes)
		Pages_Singleton.Get_ActivePage().BodyGrid.Width = ui.TermWidth()
		Pages_Singleton.Get_ActivePage().BodyGrid.Align()
		ui.Clear()
		ui.Render(Pages_Singleton.Get_ActivePage().BodyGrid)
	})
	// handle a 1s timer: refresh
	ui.Handle("/timer/1s", func(e ui.Event) {
		// Update and show active page
		Pages_Singleton.UiShow_ActivePage()

		Pages_Singleton.Get_ActivePage().BodyGrid.Align()
		ui.Render(Pages_Singleton.Get_ActivePage().BodyGrid)
	})

	// ========================
	// Finally start infinite loop
	// blocks until StopLoop is called
	ui.Loop()
	return 0
}

func lllog(val interface{}) {
	f, err := os.OpenFile("./uDash.debug", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0664)
	if err != nil {
		ui_StopClose_logPanic(err)
	}
	defer f.Close()

	_, err = f.WriteString(fmt.Sprintf("%+v\n", val))
	if err != nil {
		ui_StopClose_logPanic(err)
	}
}
func maxIntOf(v []int) int {
	if len(v) == 0 {
		ui_StopClose_logPanic("Oh no, review the code!")
	}
	m := v[0]
	for e := range v {
		if e > m {
			m = e
		}
	}
	return m
}

func getYamlFilenameFromArgs() (yaml_filepath string, err error) {
	// define default_YamlConfig_filecontent
	var default_YamlConfig_filecontent string
	{
		default_YamlConfig_filecontent = `
- page:
  - line:
    - col:
      - brick:
          BorderLabel: "Make your personal dashboard"
          Width_span1to12: 7  
          Height_percentOfTerminal: 60
          UserCommand: |
            cat <<'EOT'
            For your first dashboard:
              1 First run once ./uDash, so it creates the default TEMPLATE_FILE ([1])
              2 Copy the TEMPLATE_FILE file ([1]) to your own ./myDash.yml, like 
                   $ cp -v ~/.config/uDash/uDash.template.yml ./myDash.yml
              3 Edit ./myDash.yml and add/substitute the *UserCommand* by your own commands
              4 Run it with "./uDash ./myDash.yml"
                Every 1sec, the commands will be executed in background, and once they terminate their 
                stdout/stderr gets shown (updated) in the dashboard
  
            For your second dashboard, an improved one, step3 becomes more refined:
              + First think about the *design* you want for the dashboard: 
                + Decide what *UserCommand* you want to show, and have a rough idea of how much space they will need
                + Decide how many *pages* you want
                + Decide how many *bricks* you want in each *page*, to be able to show those UserCommand's
                + For each *page*, decide *where* you want to place each *bricket*
                + For each *page*, decide the *size* (*Height_percentOfTerminal* and *Width_span1to12*) of each *bricket*
                + For each *page*, calculate the *lines* and *cols* needed to place those *brickets*
                Its usually easier to think this with a pencil-and-paper
              + And finally, edit the .yml file and put the resulting values there
                Iterate a couple of times, to adjust the sizes, commands, and ordering
              + (Optional) Add *Highlights* to the bricks (regexp,fg-color,bg-color), like this for example:
                    red   for "error or failed"
                    green for <= "10%"
                    blue  for "intERESTing (ignore case)"
            
            Keys:
              <Left>/<Right>         : prev/next page
              <Escape>, q, Ctrl-C    : quit


            FILES:
            [1] - ~/.config/uDash/uDash.template.yml


            EOT
          Highlights:
            - Name: "for error or failed"
              MatchRegexp: error|failed
              FgColor: "fg-white,fg-bold"
              BgColor: "bg-red"
            - Name: "for <= 10%"
              MatchRegexp: \b(10|\d)%
              FgColor: "fg-white,fg-bold"
              BgColor: "bg-green"
            - Name: "for intERESTing ignore case"
              MatchRegexp: (?i)interesting
              FgColor: "fg-white,fg-bold"
              BgColor: "bg-blue"
            - Name: "*xx*"
              MatchRegexp: \*[^\*]+\*
              FgColor: "fg-green,fg-bold"
              BgColor: "bg-default"
            - Name: "$ shell-code"
              MatchRegexp: '  \$ .*'
              FgColor: "fg-cyan,fg-bold"
              BgColor: "bg-gray"
    - col:
      - brick:
          BorderLabel: "Processes"
          Width_span1to12: 5
          Height_percentOfTerminal: 60
          UserCommand: "ps -xu --forest"
          Highlights:
            - Name: "go"
              MatchRegexp: \bgo
              FgColor: "fg-white,fg-bold"
              BgColor: "bg-default"
            - Name: "current process"
              MatchRegexp: .*R\+.*
              FgColor: "fg-yellow"
              BgColor: "bg-black,bg-bold"
  - line: 
    - col:
      - brick:
          BorderLabel: "Disk stat"
          Width_span1to12: 4
          Height_percentOfTerminal: 40
          UserCommand: "df -h"
          Highlights:
            - Name: "go"
              MatchRegexp: .*\b0%.*
              FgColor: "fg-black,fg-bold"
              BgColor: "bg-default"
    - col:
      - brick:
          BorderLabel: "Command that fails"
          Width_span1to12: 3  
          Height_percentOfTerminal: 10
          UserCommand: "CommandThatFails"
      - brick:
          BorderLabel: "Command that takes 20 secs to finish execute"
          Width_span1to12: 3  
          Height_percentOfTerminal: 10
          UserCommand: "sleep 20; date"
      - brick:
          BorderLabel: "NTP sync"
          Width_span1to12: 3  
          Height_percentOfTerminal: 10
          UserCommand: "ntpstat -n"
      - brick:
          BorderLabel: "Time"
          Width_span1to12: 3  
          Height_percentOfTerminal: 10
          UserCommand: "date"
    - col:
      - brick:
          BorderLabel: "Scocket stats"
          Width_span1to12: 5  
          Height_percentOfTerminal: 20
          UserCommand: "ss  --numeric --processes --all --tcp"
      - brick:
          BorderLabel: "Who is logged in"
          Width_span1to12: 5  
          Height_percentOfTerminal: 20
          UserCommand: "last --since yesterday --limit 5  --time-format iso"
- page:
  - line:
    - col:
      - brick:
          BorderLabel: "Example A:"
          Width_span1to12: 12
          Height_percentOfTerminal: 100
          UserCommand: echo "line 1, col 1, brick 1"
- page:
  - line:
    - col:
      - brick:
          BorderLabel: "Example B:"
          Width_span1to12: 12
          Height_percentOfTerminal: 50
          UserCommand: echo "line 1, col 1, brick 1"
  - line:
    - col:
      - brick:
          BorderLabel: "Example B:"
          Width_span1to12: 6
          Height_percentOfTerminal: 50
          UserCommand: echo "line 2, col 1, brick 1"
    - col:
      - brick:
          BorderLabel: "Example B:"
          Width_span1to12: 3
          Height_percentOfTerminal: 50
          UserCommand: echo "line 2, col 2, brick 1"
    - col:
      - brick:
          BorderLabel: "Example B:"
          Width_span1to12: 3
          Height_percentOfTerminal: 30
          UserCommand: echo "line 2, col 3, brick 1"
      - brick:
          BorderLabel: "Example B:"
          Width_span1to12: 2
          Height_percentOfTerminal: 20
          UserCommand: echo "line 2, col 3, brick 2"
- page:
  - line:
    - col:
      - brick:
          BorderLabel: "Help"
          Width_span1to12: 12
          Height_percentOfTerminal: 100
          UserCommand: |
            cat <<EOT

            == USAGE ==

              To *quit*:                        ESCAPE, CTRL-C, Q
              To *cycle to next/prev page*:     <RIGHT-KEY> and <LEFT-KEY>

            EOT
          Highlights:
            - Name: "Blue"
              MatchRegexp: ==.*==
              FgColor: "fg-blue,fg-bold"
              BgColor: "bg-default"
  

#- page:
#  ...
#- page: 
#  - line:
#  ...
#  - line:
#    - col:
#    ...
#    - col:
#      - brick:
#      ...
#      - brick:
#          BorderLabel: "Socket stats"
#  
#          Width_span1to12: 7  
#              # 1 to 12
#              # Terminal width is divided in 12 spans (like vertical-slices) - see [1]
#              # This value, is how many spans (width) the brick should occupy
#              # Ie, its the "width" of the brick, in "spans"
#              # NOTE: The sum-of-spans-of-all-columns-in-same-line should be <= 12
#              #
#              # 12 spans is the full-width of the terminal
#              # 6  spans is the half-width of the terminal
#              # 3  spans is the 1/4 width of the terminal
#              # etc
#              #
#              # [1] the 12 columns grid system  http://www.w3schools.com/bootstrap/bootstrap_grid_system.asp
#  
#          Height_percentOfTerminal: 25 
#              # 1 to 100
#              # NOTE: The sum-of-heights-of-all-lines should be <= 100
#
#
#  
#          UserCommand: "ss  --numeric --processes --all --tcp --udp --summary"
#              # The UserCommand is executed every 1sec, by /bin/bash -c <UserCommand>
#              # and when it terminates its stderr/stdout are displayed
#              #
#              # As it is run via bash, you can (more-or-less) expect to see the same result as when you 
#              # run yourself the UserCommand manually in you bash session, which is helpfull 
#              # for trying/testing new commands
#              #
#              # The dashboard/brick cannot interact with the UserCommand while it is running.
#              # Ie, this is not interactive - it works more like this:
#              #       .in background, its executed the UserCommand via bash -c "<UserCommand>"
#              #       .in background, its waited untill it finishes executing and then its captured the resulting stderr/stdout
#              #       .Every 1sec, its checked if the command finished. If its finished then its displayed the 
#              #        last captured stderr/stdout in the uDash (brick), and the command is re-launched again in background
#              #
#              # Example: 
#              #    UserCommand: "ls -l | tr [:lower:] [:upper:]"
#              #    
#              #    will be executed as     bash -c "ls -l | tr [:lower:] [:upper:]"
#
#
#  
#          # (Optional) the section Highlights is optional. Delete it from the brick if you dont want it
#          Highlights:
#            - Name: this highlight name
#            ...
#            - Name: "error or failed"
#                  # Name is just a comment for the user, can be whatever you want
#
#              MatchRegexp: error|failed
#                  # MatchRegexp - a regexp of what will be highlighted
#                  #   The regexp is like Perl/Python, see https://golang.org/pkg/regexp/#pkg-overview for details
#                  #   Test your regexps in https://regex-golang.appspot.com/assets/html/index.html
#                  #   LIMITATIONS AND KNOWN-ISSUES:
#                  #     + regexp is always evaluated in one-line-of-text (not multiline). 
#                  #       So .*xxx.*  match one entire line that contains xxx
#                  #     + $ and ^ dont work as expected, but \n can be used
#                  #       The ^ only matches the beginning-of-first-line-of-the-text - so it works on first-line, and does not work on all other lines
#                  #       Try to use (^|\n) instead of ^
#                  #       The $ only matches the end-of-last-line-of-the-text - so it works on last-line, and does not work on all other lines
#                  #       Try to use \n instead of $
#                  #     + If you have errors with the regexp, try surrounding (or un-surrounding) it with "" and ''
#                  #     + Dont use multiple highlights on same text, does not work! 
#                  #        Only first highlight is applied and garbage-characters will be introduced, dont do it
#  
#              FgColor: "fg-white,fg-bold"
#              BgColor: "bg-red"
#                  # FgColor can be one of:
#                  #    FgColor: "fg-XXX"
#                  #    or
#                  #    FgColor: "fg-XXX,fg-YYY"
#                  #
#                  # BgColor can be one of:
#                  #    BgColor: "bg-XXX"
#                  #    or
#                  #    BgColor: "bg-XXX,bg-YYY"
#                  #
#                  #   where:
#                  #        XXX = 
#                  #              red    
#                  #              blue   
#                  #              black  
#                  #              cyan   
#                  #              yellow 
#                  #              white  
#                  #              default
#                  #              green  
#                  #              magenta
#                  #        YYY = 
#                  #              bold      
#                  #              underline 
#                  #              reverse  
#                  #    
#
#
#
#  
#  
#   NOTE about yaml (this file): 
#    + cannot contain any <Tabs> or an error like this will show up:
#        line xx: found character that cannot start any token
#
#    + Avoid using : if possible (or surround it with quotes ":" ':')

`
	}

	// define default_YamlConfig_dir and default_YamlConfig_filepath
	var default_YamlConfig_dir string
	var default_YamlConfig_filepath string
	{
		usr, err := user.Current()
		if err != nil {
			return yaml_filepath, err
		}
		default_YamlConfig_dir = usr.HomeDir + "/.config/uDash"

		default_YamlConfig_filepath = default_YamlConfig_dir + "/uDash.template.yml"
	}

	// mkdir default_YamlConfig_dir if it does not exist yet
	{
		err := os.MkdirAll(default_YamlConfig_dir, 0775)
		if err != nil {
			return yaml_filepath, err
		}
	}

	// create default_YamlConfig_filepath if it does not exist yet
	{
		if _, fake_err := os.Stat(default_YamlConfig_filepath); os.IsNotExist(fake_err) {

			// default_YamlConfig_filepath does not exist
			err = ioutil.WriteFile(default_YamlConfig_filepath, []byte(default_YamlConfig_filecontent), 0644)
			if err != nil {
				return yaml_filepath, err
			}
		}
	}

	// define yaml_filepath
	{
		// If arg1 was supplied by user, then define yaml_filepath from arg1
		// Else, yaml_filepath = default_YamlConfig_filepath (creating it if needed)
		//
		// NOTE: in this code-block we just derive the  yaml_filepath but we dont check if it exists or not

		var arg1 string
		if len(os.Args) >= 2 {
			// The arg1 was supplied by user
			//   a) arg1 might be a relative/fullpath to a file.
			//			"c/d/file.yml"
			//			"/a/b/c/d/file.yml"
			//			In this case: yaml_filepath = arg1
			//
			//	 b) arg1 might be just someFilename corresponding to a file existing in default_YamlConfig_dir
			//			myFile     -->> default_YamlConfig_dir/myFile.yml
			//			In this case: yaml_filepath = default_YamlConfig_file/arg1.yml
			arg1 = os.Args[1]

			if strings.Contains(arg1, "/") == true {
				//   a) arg1 might be a relative/fullpath to a file.
				//			"c/d/file.yml"
				//			"/a/b/c/d/file.yml"
				//			In this case: yaml_filepath = arg1
				yaml_filepath = arg1
			} else {
				//	 b) arg1 might be just someFilename corresponding to a file existing in default_YamlConfig_dir
				//			myFile     -->> default_YamlConfig_dir/myFile.yml
				//			In this case: yaml_filepath = default_YamlConfig_file/arg1.yml
				yaml_filepath = default_YamlConfig_dir + "/" + arg1 + ".yml"
			}

		} else {
			// Else, yaml_filepath = default_YamlConfig_filepath (creating it if needed)
			yaml_filepath = default_YamlConfig_filepath
		}
	}
	return yaml_filepath, nil
}
